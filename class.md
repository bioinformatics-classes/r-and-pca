### classes[16] = "Principal Component Analyses in R"

#### Análise de Sequências Biológicas 2023-2024

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

---

## An intro to "GNU R"

![R logo](assets/R_logo.png)

[GNU R](https://www.r-project.org/)

---

### Meet R

![A linux console running R](assets/R-console.gif)

---

### RStudio - an R IDE

![RStudio_image](assets/rstudio.png)

* &shy;<!-- .element: class="fragment" --> Text editor (Top left)
* &shy;<!-- .element: class="fragment" --> Console (Bottom left)
* &shy;<!-- .element: class="fragment" --> Workspace objects (Top right)
* &shy;<!-- .element: class="fragment" --> Multi-purpose (Bottom right)

---

### How do I use this thing?

#### Calculator

<div class="fragment">

``` R
4 + 4
[1] 8

(2 + 5) * 6 / (6 - 2)
[1] 10.5
```

</div>

|||


### How do I use this thing?

#### Text processor

<div class="fragment">

``` R
print("Good Morning World!")
[1] "Good Morning World!"
```

</div>

&shy;<!-- .element: class="fragment" -->![Bart Simpson saying "Good morning world!"](assets/BS_good_morning.gif)

|||

### How do I use this thing?

``` R
2 < 3  # Logical operators

2 > 3

2 <= 3

2 >= 3

2 == 2

2 == 2 | 2 >= 3  # OR

2 == 2 & 2 < 3  # AND
```

---

### R programming 101

#### Variable assignment

```R
txt = "Good Morning World!"
txt <- "Good Morning World!"
print(txt)
```

---

### "Simple" data types

* &shy;<!-- .element: class="fragment" --> Numeric (1.5) - AKA "Float"
* &shy;<!-- .element: class="fragment" --> Integer (1) - these have to be coerced
* &shy;<!-- .element: class="fragment" --> Complex (1+2i) - for imaginary numbers
* &shy;<!-- .element: class="fragment" --> Logical (TRUE) - or FALSE
* &shy;<!-- .element: class="fragment" --> Character ("ABC") - AKA "String"

|||

### "Simple" data types

``` R
number = 1.5
class(number)
```

* &shy;<!-- .element: class="fragment" --> Try this with other variable types
* &shy;<!-- .element: class="fragment" -->[Would you like to know more?](https://stackoverflow.com/questions/35445112/what-is-the-difference-between-mode-and-class-in-r)

---

### 'Advanced' data types

#### Vector

A **vector** is a **sequence** of data elements **of the same basic type**. Members in a vector are called "components". It is defined using `c()`:

<div class="fragment">

``` R
c(1, 2, 1)

c("AA", "Aa", "aa")

c(TRUE, FALSE, TRUE)
```

</div>

|||

### Vector

We can perform a lot of operations on vectors:

<div class="fragment">


``` R
a = c(1, 3, 5, 7)
a * 5  # Multiplication
b = c(1, 2, 4, 8) 
a + b  # Arithmetics with vectors
d = c(1, 2)
a + d  # Recycling rule!
a[2]  # Indexing
```

</div>

---

### "Advanced" data types

#### Factor

Factors are used for storing **categorical variables**.

![Pokéball](assets/pokeball.png)

<div class="fragment">

``` R
captured_pokemon = c("normal", "normal", "electric", "fire", "fire", "fire", "water", "grass")
captured_pokemon_types = factor(captured_pokemon)
levels(captured_pokemon_types)
```

</div>

---

### 'Advanced' data types

#### Matrix

A **matrix** is a collection of **data elements** arranged in a **two-dimensional rectangular layout**. It is defined using a `function`.

```R
my_matrix = matrix(c(1, 2, 3, 4, 5, 6),  # data elements
                   nrow=2,  # number of rows
                   ncol=3)  # number of columns
print(my_matrix)

my_matrix[1,2]  # Bidimensional indexing!
my_matrix[1,]  # Get a single row
my_matrix[,1]  # Get a single column
```

---

### 'Advanced' data types

#### List

A list is a generic vector containing other objects

``` R
n = c(2, 3, 5) 
s = c("aa", "bb", "cc", "dd", "ee") 
b = c(TRUE, FALSE, TRUE, FALSE, FALSE) 
x = list(n, s, b, 3)  # x contains copies of n, s, b 
```

---

### 'Advanced' data types

#### Data Frame

A **data frame** is used for storing data tables. It is a set of vectors of equal length.

``` R
n = c(2, 3, 5) 
s = c("aa", "bb", "cc") 
b = c(TRUE, FALSE, TRUE) 
dframe = data.frame(n, s, b)  # dframe is a data frame 
```

|||

### 'Advanced' data types

#### Data Frame

``` R
mtcars  # Built in data! 
head(mtcars)  # Try this instead...
View(mtcars)  # Or this
mtcars[1, 2]  # Indexing
mtcars["Mazda RX4", "cyl"]  # Named indexing!

# All these get the same data (sort of)
mtcars[[9]]  # Get column 9
mtcars[, 9]  # Also get column 9
mtcars[["am"]]  # Get column "am"
mtcars$am  # Alternative - notice the lack of quotes
mtcars[, "am"]  # Another alternative - arguably the most common
mtcars["am"]  # Calling it like this will get the names too

# Same thing, but for rows
mtcars[1, ]  # Get row 1
mtcars["Mazda RX4", ]

# Logical indexing!!!111!!one
mtcars[, "mpg"] <= 15

# Rows and columns can be combined:
mtcars["Mazda RX4", "mpg"]  # The Japenese car's fuel consumption
mtcars[mtcars[, "cyl"] == 6, ]  # Can you guess what this does?
```

---

### Functions

* &shy;<!-- .element: class="fragment" -->Invoked by their **name**
    * &shy;<!-- .element: class="fragment" -->Followed by parenthesis `()`
        * &shy;<!-- .element: class="fragment" -->Containing zero or more arguments

<div class="fragment">

```R
c(1, 2, 3)  # `c` actually stands for "combine"

sum(1, 2)  # A sum function

head(mtcars)  # Show the first few lines of...

seq(10, 30)  # Create a sequence (start, end, [step])
seq(10, 30, 2)  # See "step" in action
```

</div>

* &shy;<!-- .element: class="fragment" -->We can also define our own functions.

&shy;<!-- .element: class="fragment" -->![Aragorn - "Not this day!"](assets/not_this_day.jpg)

|||

### Functions

``` R
# See also

mean()
sd()
max()
min()
summary()
table()
```

---

### Data coercion (AKA *Typecasting*)

* There are functions than can be used to transform variables:

``` R
as.data.frame(MATRIX)

as.numeric(INTEGER)

as.character(NUMERIC)

# Many more!
```

---

### Getting help

* "When in doubt, ask."
  * &shy;<!-- .element: class="fragment"-->Ask R for help with the command `?function`
  * &shy;<!-- .element: class="fragment"-->If you need help on a **topic**, use the `??` notation ("fuzzy search")

<div class="fragment">

```R
?head

??multivariate
```

</div>

---

### Installing external packages

* &shy;<!-- .element: class="fragment"-->One of R's greatest strengths is how many 3rd party packages are developed for it
  * &shy;<!-- .element: class="fragment"-->Installing these external packages is as easy as typing `install.packages(pkg_name)`
  * &shy;<!-- .element: class="fragment"-->We then have to load the packages we need in order to use them

<div class="fragment">

``` R
install.packages("psych")

library("psych")
```

</div>

---

### Missing data

Missing data is coded in R as `NA`, which stands for "**N**ot **A**vailable"

<div class="fragment">


``` R
incomplete_vector = c(1, 2, NA, 4)
is.na(incomplete_vector)

mean(incomplete_vector)

mean(incomplete_vector, na.rm=TRUE)
```

</div>

---

### Loading data from external sources

* &shy;<!-- .element: class="fragment"-->You don't have to type data into R manually
  * &shy;<!-- .element: class="fragment"-->You can load data directly from files
  * &shy;<!-- .element: class="fragment"-->Use `read.csv()` or `read.table()`

<div class="fragment">


``` R
data = read.csv("/path/to/file.csv")

data = read.table("/path/to/file.txt", header=FALSE, sep="\t")  # Harder to use alternative

data = read.csv("https://some.site.with.data.com/datafile.txt", header=TRUE, sep=";", dec=",")  # URLs are supported!
```

---

### 5' Break

---

### Exploratory data analysis

### &shy;<!-- .element: class="fragment" -->PCA

---

### What is a PCA?

* &shy;<!-- .element: class="fragment" -->"**P**rincipal **C**omponent **A**nalysis"
* &shy;<!-- .element: class="fragment" -->Ordination method
* &shy;<!-- .element: class="fragment" -->Exploratory
* &shy;<!-- .element: class="fragment" -->A dimensional reduction technique
* &shy;<!-- .element: class="fragment" -->A way to find "hidden" patterns in large, complex, datasets

---

### When is a PCA useful?

* &shy;<!-- .element: class="fragment" -->If data simplification is required
* &shy;<!-- .element: class="fragment" -->If variables are highly correlated
* &shy;<!-- .element: class="fragment" -->If dealing with 3 dimensional or higher data
* &shy;<!-- .element: class="fragment" -->As a starting point for other analyses

---

### How does a PCA 'work'?

* &shy;<!-- .element: class="fragment" -->Uses orthogonal transformation
* &shy;<!-- .element: class="fragment" -->Converts possibly correlated variables into uncorrelated
* &shy;<!-- .element: class="fragment" -->The new variables are called "principal components" (PCs)
* &shy;<!-- .element: class="fragment" -->The PCs are sorted by the amount of variability they explain

&shy;<!-- .element: class="fragment" -->[Would you like to know more?](https://tgmstat.wordpress.com/2013/11/21/introduction-to-principal-component-analysis-pca/)

---

### PCAs can be performed in "plain" R, but let's take it up a notch

#### Introducing <!-- .element: class="fragment" data-fragment-index="1" --> 

[BioConductor](https://bioconductor.org/) <!-- .element: class="fragment" data-fragment-index="1" -->

|||

### When "vanilla" R just isn't enough

* &shy;<!-- .element: class="fragment" -->One of the good things about R is it's expandability
* &shy;<!-- .element: class="fragment" -->It is possible to "import" thousands of external 3rd party packages
* &shy;<!-- .element: class="fragment" -->Bioconductor is more than just a package. It's a 3rd party package repository
  * &shy;<!-- .element: class="fragment" -->It hosts ~~1473~~ ~~1649~~ ~~1823~~ ~~1974~~ ~~2042~~ ~~2143~~ ~~2266~~ 2300 bioinformatics related packages (at the time of writing)
  * &shy;<!-- .element: class="fragment" -->It is very easy to use directly from R

---

### Trying out "pcaMethods" package

* Install and load the new library

```R
install.packages("BiocManager")
BiocManager::install("pcaMethods")
library(pcaMethods)
```

---

### A simple example

* &shy;<!-- .element: class="fragment" -->Suppose we have some students and their respective grades
  * &shy;<!-- .element: class="fragment" -->We need to group students based on their grades

|||

### Showtime!

Let's try to group some students.
Get the data and take a look at what we have obtained.

``` R
student_df = read.csv("https://gitlab.com/bioinformatics-classes/r-and-pca/raw/master/assets/students.csv",
                      header=TRUE,
                      row.names=1,
                      sep=";")
View(student_df)
```

|||

### Showtime!

Let's look at our student's grades using PCA

```R
library(pcaMethods)

# Calculate the PCA
studentPCA = pca(student_df[,1:4], scale="vector", center=T, nPcs=2, method="svd")  # Why not the entire DF?

# Get the scores
scores_pc1 = studentPCA@scores[, "PC1"]
scores_pc2 = studentPCA@scores[, "PC2"]

# Draw the plot
plot(scores_pc1, scores_pc2, main="Student grades' PCA plot")

# Optionally label each point
text(scores_pc1, scores_pc2, rownames(student_df), pos= 3 )
```

|||

### Showtime!

Let's add some color!
Using the "Universe" as the discriminant

```R
# Define colours for the "Universe" grouping
my_categories = unique(student_df[,"Universe"])
universe_colours = as.numeric(factor(student_df[,"Universe"],
                                     levels=my_categories))

# Draw the plot again
plot(scores_pc1, scores_pc2, main="Student grades' PCA plot",
     col=universe_colours)

# Optionally label each point
text(scores_pc1, scores_pc2, rownames(student_df), pos= 3 )

# Draw a nice looking legend
legend("topright", legend=my_categories, pch = 1,
       col=unique(universe_colours))
```

|||

### Showtime!

Lets try again
Using the "Category" as the discriminant

```R
# Do the same, but for another discriminant
my_categories = unique(student_df[,"Category"])
category_colours = as.numeric(factor(student_df[,"Category"],
                                     levels=my_categories))

plot(scores_pc1, scores_pc2, main="Student grades' PCA plot",
     col=category_colours)
text(scores_pc1, scores_pc2, rownames(student_df), pos= 3 )

legend("topright", legend=my_categories, pch = 1,
       col=unique(category_colours))
```

---

### Let's scale things up a bit...

```R
library(pcaMethods)
# Get some data
wine <- read.csv("http://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data", sep=",")

# Add column names
colnames(wine) <- c("Cultivar", "Alcohol", "Malic acid", "Ash",
                    "Alcalinity of ash", "Magnesium", "Total phenols",
                    "Flavanoids", "Nonflavanoid phenols", "Proanthocyanins",
                    "Color intensity", "Hue", "OD280/OD315 of diluted wines",
                    "Proline")

# The first column corresponds to the cultivar class
cultivar_classes <- factor(wine$Cultivar)

winePCA <- pca(wine[, -1], scale="vector", center=T, nPcs=2, method="svd" )

scores_pc1 = winePCA@scores[, "PC1"]
scores_pc2 = winePCA@scores[, "PC2"]

plot(scores_pc1, scores_pc2,
     col=cultivar_classes,
     main="Wine PCA plot")
legend("bottomright", legend = c("Cv1", "Cv2", "Cv3"), pch = 1,
       col = c("black", "red", "green"))
```

|||

### Squeeze more information!

We can add the percentage of explained variation:

```R
plot(scores_pc1, scores_pc2,
     col=cultivar_classes,
     ann=FALSE)

title(main="Wine PCA plot")
title(xlab=sprintf("PC1 %0.1f%% variation explained", round(winePCA@R2[1] * 100, 2)))
title(ylab=sprintf("PC2 %0.1f%% variation explained", round(winePCA@R2[2] * 100, 2)))

legend("bottomright", legend = c("Cv1", "Cv2", "Cv3"), pch = 1,
       col = c("black", "red", "green"))
```

|||

### How are variables influencing each PC?

A "Loadings plot" will allow us to check that

```R
loadings_pc1 = winePCA@loadings[, "PC1"]
loadings_pc2 = winePCA@loadings[, "PC2"]
plot(loadings_pc1, loadings_pc2, pch="", ann=F)
arrows(0, 0, loadings_pc1, loadings_pc2)

text(loadings_pc1, loadings_pc2,
     rownames(winePCA@loadings), cex=0.9)
title(xlab=sprintf("PC1 %0.1f%% variation explained", round(winePCA@R2[1] * 100, 2)))
title(ylab=sprintf("PC2 %0.1f%% variation explained", round(winePCA@R2[2] * 100, 2)))
title(main="Wine PCA loadings plot")
```

[Source](https://www.r-bloggers.com/principal-component-analysis-in-r/)

|||

### Still skeptic?

Let's test it!

```R
shapiro.test(wine$Ash[wine$Cultivar == "1"])
shapiro.test(wine$Ash[wine$Cultivar == "2"])
shapiro.test(wine$Ash[wine$Cultivar == "3"])

t.test(x=wine$Ash[wine$Cultivar == "2"], y=wine$Ash[wine$Cultivar == "3"])
t.test(x=wine$Ash[wine$Cultivar == "2"], y=wine$Ash[wine$Cultivar == "1"])

# What about this one?
t.test(x=wine$Ash[wine$Cultivar == "3"], y=wine$Ash[wine$Cultivar == "1"])
```
---

### Genomics data!

* &shy;<!-- .element: class="fragment" -->We will be using "real" data now
* &shy;<!-- .element: class="fragment" -->Obtained from RAD-Seq sequencing of *Timon lepidus*
* &shy;<!-- .element: class="fragment" -->Was already assembled, filtered and QC'ed for use
* &shy;<!-- .element: class="fragment" -->Is capped to 2000 SNPs and 26 individuals
 * &shy;<!-- .element: class="fragment" -->Originally was 15322 SNPs and 46 individuals

&shy;<!-- .element: class="fragment" -->![Picture of DNA](assets/genome.jpg)

---

### Real world example

* &shy;<!-- .element: class="fragment" -->Data in [STRUCTURE](https://web.stanford.edu/group/pritchardlab/structure_software/release_versions/v2.3.4/structure_doc.pdf) format
 * &shy;<!-- .element: class="fragment" -->A row with marker names
 * &shy;<!-- .element: class="fragment" -->One row per individual with SNP data
 * &shy;<!-- .element: class="fragment" -->"A", "C", "G" and "T" encoded as "1", "2", "3" and "4" respectively
 * &shy;<!-- .element: class="fragment" -->Missing data encoded as "NA" or "-9"
 * &shy;<!-- .element: class="fragment" -->All fields are space delimited
* &shy;<!-- .element: class="fragment" -->In this case, the first 4 columns are "Individual ID", "Population name", "Population number" and "Age"
* &shy;<!-- .element: class="fragment" -->Keep in mind that this format is **very** flexible
* &shy;<!-- .element: class="fragment" -->[The data](assets/TLE.str)

---

### Your task for today:

* Perform a PCA analysis of the *T. lepidus* dataset
* Create both a *scores* and a *loadings* plot
* Try using `Age` and `Population` as a discriminant

---

### References

* [GNU R](https://www.r-project.org/)
* [RStudio](https://posit.co/products/open-source/rstudio/)
* [Advanced R: Data structures](http://adv-r.had.co.nz/Data-structures.html)
* [The Comprehensive R Archive Network](https://cran.r-project.org/)
* [Mode Vs. Class in R](https://stackoverflow.com/questions/35445112/what-is-the-difference-between-mode-and-class-in-r)
* [Brief Intro to PCA](https://tgmstat.wordpress.com/2013/11/21/introduction-to-principal-component-analysis-pca/)
* [Performing a PCA with wine data](https://www.r-bloggers.com/principal-component-analysis-in-r/)
* [STRUCTURE format](https://web.stanford.edu/group/pritchardlab/structure_software/release_versions/v2.3.4/structure_doc.pdf)
